---
sidebar_position: 1
---

# Preparation

## OPAM or Nix?

As an advanced research tool, EasyCrypt has complex dependencies, that are best
managed by an established toolchain.

- `opam` is the Ocaml PAckage Manager. It is easy to set up, but may be more
  difficult to manage through updates and upgrades. `opam` does not need
  administrative rights, although the installation of `opam` itself is easiest
  done with administrative rights.

- `nix` is a declarative package manager aiming at providing reproducible
  builds. It is harder to set up initially, but the cost of managing it is on
  us once you have things set up. It is possible to set `nix` up so that
  continued usage does not require administrative rights.

At this stage, this is all I'm going to be writing about this, so head over to
the [existing
README](https://github.com/EasyCrypt/easycrypt/blob/main/README.md), and hope
that all goes well. This will be expanded later on.
