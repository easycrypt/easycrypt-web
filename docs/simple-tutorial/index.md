---
slug: /simple-tutorial
---

# A Simple Proof — IND$-NRCPA-Secure Nonce-Based Symmetric Encryption Scheme

As a warm-up, we'll consider a simple nonce-based symmetric encryption scheme
for fixed-length messages.[^1] Here, we assume some familiarity with
cryptographic definitions and proofs.

```mdx-code-block
import DocCardList from '@theme/DocCardList';
import {useCurrentSidebarCategory} from '@docusaurus/theme-common';

<DocCardList items={useCurrentSidebarCategory().items}/>
```

[^1]: Note the similarities to The Joy of Cryptography's [Construction 7.4][0].

[0]: https://joyofcryptography.com/pdf/chap7.pdf#page=7