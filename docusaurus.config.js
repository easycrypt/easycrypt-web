// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer').themes.github;
const darkCodeTheme = require('prism-react-renderer').themes.dracula;

const math = require('remark-math');
const katex = require('rehype-katex');

/** @type {import('@docusaurus/types').Config} */
export default async function createConfigAsync() {

  return {
    title: 'EasyCrypt',
    tagline: 'Machine-Checked Cryptographic Proofs',
    url: 'https://easycrypt.gitlab.io',
    baseUrl: '/easycrypt-web/',
    onBrokenLinks: 'throw',
    onBrokenMarkdownLinks: 'warn',
    favicon: 'img/favicon.ico',

    // GitHub pages deployment config.
    // If you aren't using GitHub pages, you don't need these.
    organizationName: 'EasyCrypt', // Usually your GitHub org/user name.
    projectName: 'easycrypt', // Usually your repo name.

    // Even if you don't use internalization, you can use this field to set useful
    // metadata like html lang. For example, if your site is Chinese, you may want
    // to replace "en" with "zh-Hans".
    i18n: {
      defaultLocale: 'en',
      locales: ['en'],
    },

    presets: [
      [
        'classic',
        /** @type {import('@docusaurus/preset-classic').Options} */
        ({
          docs: {
            sidebarPath: require.resolve('./sidebars.js'),
            editUrl:
              'https://gitlab.com/easycrypt/easycrypt-web/',
            remarkPlugins: [math],
            rehypePlugins: [katex],
          },
          //blog: {
          //  showReadingTime: true,
          //  // Please change this to your repo.
          //  // Remove this to remove the "edit this page" links.
          //  editUrl:
          //    'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
          //},
          theme: {
            customCss: require.resolve('./src/css/custom.css'),
          },
        }),
      ],
    ],

    stylesheets: [
      {
        href: 'https://cdn.jsdelivr.net/npm/katex@0.13.24/dist/katex.min.css',
        type: 'text/css',
        integrity:
          'sha384-odtC+0UGzzFL/6PNoE8rX/SPcQDXBJ+uRepguP4QkPCm2LBxH3FA3y+fKSiJ+AmM',
        crossorigin: 'anonymous',
      },
    ],

    themeConfig:
      /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
      ({
        navbar: {
          title: 'EasyCrypt',
          logo: {
            alt: 'EasyCrypt Logo',
            src: 'img/logo.svg',
          },
          items: [
            {
              type: 'doc',
              docId: 'intro',
              position: 'left',
              label: 'Tutorial',
            },
            //{to: '/blog', label: 'Blog', position: 'left'},
            //{
            //  type: 'doc',
            //  docId: 'howto',
            //  position: 'left',
            //  label: 'How To',
            //},
            //{
            //  type: 'doc',
            //  docId: 'reference',
            //  position: 'left',
            //  label: 'Reference',
            //},
            {
              href: 'https://github.com/EasyCrypt/easycrypt',
              label: 'GitHub',
              position: 'right',
            },
          ],
        },
        footer: {
          style: 'dark',
          links: [
            {
              title: 'Docs',
              items: [
                {
                  label: 'Tutorial',
                  to: '/docs/intro',
                },
              ],
            },
            {
              title: 'Community',
              items: [
                {
                  label: 'Zulip',
                  href: 'https://formosa-crypto.zulipchat.com/#narrow/stream/308381-EasyCrypt.3A-questions-.26-features',
                },
                //{
                //  label: 'Discord',
                //  href: 'https://discordapp.com/invite/docusaurus',
                //},
                //{
                //  label: 'Twitter',
                //  href: 'https://twitter.com/docusaurus',
                //},
              ],
            },
            {
              title: 'More',
              items: [
                //{
                //  label: 'Blog',
                //  to: '/blog',
                //},
                {
                  label: 'GitHub',
                  href: 'https://github.com/easycrypt/easycrypt',
                },
              ],
            },
          ],
          copyright: `Copyright © ${new Date().getFullYear()} EasyCrypt. Built with Docusaurus.`,
        },
        prism: {
          theme: lightCodeTheme,
          darkTheme: darkCodeTheme,
        },
      }),

    plugins: [
        require.resolve('docusaurus-lunr-search'),
    ],
  };
};
