
## Interlude: Modules and Procedures, Part 2
As alluded to in [the first part of this memo][0], EasyCrypt allows modules to be parameterized on other modules; 
such modules are called *higher-order modules* or *functors*. (The module parameters themselves cannot be higher-order modules.)
The procedures of higher-order modules may refer to the exposed procedures of their module parameters; in principle, the available 
procedures of a module parameter are the ones defined in its module type. Otherwise, higher-order modules are nearly identical to
regular modules; this is reflected in the typical form of their definitions, as depicted in the following snippet (compare this to the typical
form of the definitions of regular modules and their types in [the previous part][0]).

```
module type HOMT(M1 : MT1, M2 : MT2, ..., Mn : MTn) = {
  proc p(id1 : t1, id2 : t2, ..., idn : tn) : tout
  (* ... Other procedure signatures ... *)
}.

module (HOM : HOMT) (M1 : MT1, M2 : MT2, ..., Mn : MTn) = {
  var id : t
  (* ... Other module-level variable declarations ... *)

  proc p(id1 : t1, id2 : t2, ..., idn : tn) : tout = {
    (* ... Implementation of procedure p (may call procedures of M2, ..., Mn) ... *)
  } 

  (* ... Other procedure implementations (may call procedures of M2, ..., Mn) ... *)
}.
```
Here, notice the type denotation for `HOM`: it is given before the declaration of the module parameters.
Intuitively, this is because, for the typing of modules, we consider the module parameters as "given" or "instantiated".
Thus, `HOM` is only of type `HOMT` before the declaration of the module parameters, but not after; in fact, *after* the 
declaration of the module parameters, each of the parameters is considered "instantiated" (in terms of typing), and `HOM` 
is not of a functor type anymore. The following snippet illustrates this further. 

```
module type HOMT(M1 : MT1, M2 : MT2, ..., Mn : MTn) = {
  proc p(id1 : t1, id2 : t2, ..., idn : tn) : tout
}.

module type MT = {
  proc p(id1 : t1, id2 : t2, ..., idn : tn) : tout
}.

module (HOM : HOMT) (M1 : MT1, M2 : MT2, ..., Mn : MTn) : MT = {
  proc p(id1 : t1, id2 : t2, ..., idn : tn) : tout = {
    (* ... Implementation of procedure p (may call procedures of M2, ..., Mn) ... *)
  }
}.
```

It is possible to partially apply functors from left to right—that is, only instantiate 
the first couple of parameters, but not all—producing a new functor (with the remaining non-instantiated 
module parameters as parameters). Nevertheless, it is only possible to refer to the procedures of 
fully applied functors; otherwise, procedures may not be well-defined (e.g., calling the procedure of a 
non-instantiated module parameter). The following snippet provides an example of partial functor application.

```
module (HOM : HOMT) (M1 : MT1, M2 : MT2, ..., Mn : MTn) = {
  (* ... *)
}.

module M1 : MT1 = {
  (* ... *)
}.

(* HON is a functor parameterized on modules of type MT2, ..., MTn, in that order. *)
module HON = HOM(M1).
```
